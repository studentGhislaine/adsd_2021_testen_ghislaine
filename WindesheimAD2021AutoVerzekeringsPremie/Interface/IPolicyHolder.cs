﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Interface
{
    interface IPolicyHolder
    {
        public int Age { get; }
        public int LicenseAge { get; }
        public int PostalCode { get; }
        public int NoClaimYears { get; }
    }
}
