# ADSD_2021_Testen_Ghislaine

**DEEL 1**

**Beschrijving van de unittesten die ik heb gemaakt:**

- Het doel van de unittest
- Waarom heb ik voor deze specifieke (data) invulling van de unittest gekozen
- Welke technieken heb ik gebruikt om tot deze data te komen


**PremiumCalculationTest**

**WAInsuranceCoverageCanBeCalculatedCorrectly()**

Ik heb hierboven getest of de basispremie (WA) op basis van alleen de voertuigeisen goed berekend kan worden. 
Ik heb de gegevens van het voertuig gemockt, zodat de test, ongeacht het bouwjaar van de auto, niet faalt. De rest van de voertuigwaarden (zoals de waarde of PK) kan gemanipuleerd worden om de correcte basis premie te checken. Ik heb voor Fact gekozen, omdat ik alleen de ‘kale’ basispremie wilde testen en niet andere beïnvloedende zaken als de verzekerde. 

**PolicyHolderUnder23YearsOldGetsHigherBasePremium()** 

Hier heb ik getest of de leeftijd van de verzekerde correct werkt, waarbij iemand jonger dan 23 een duurdere premie betaalt dan iemand van 23 jaar of ouder. Zowel het voertuig als andere verzekerde-factoren heb ik gemocht, behalve zijn leeftijd.
Ook hier heb ik een Fact gebruikt, om de onder- en bovengrens te testen. Achteraf was een Theory misschien beter geweest, zodat ik ok de bovengrens kan testen, aangezien Fact mij alleen maar twee waarden kan laten testen, omdat een derde (leeftijd van 24 jaar) niet werd meegenomen in de test, toen ik die toevoegde.
 
**LicenseAgeUnder5YearsLeadsToHigherPremium()**

Met deze test heb ik op dezelfde manier getest als de leeftijdstest hierboven. Ik heb getest of de rijbewijsleeftijd goed berekend werd. Hier geldt hetzelfde als hierboven beschreven.

**PremiumPaymentPerYearGivesTwoPointFivePercentDiscount(PremiumCalculation.PaymentPeriod paymentPeriod, double expectedPremiumAmountPerYear)**

Deze test onderzoekt of je daadwerkelijk bij een jaarbetaling van de premie 2.5% korting krijgt, dan wanneer je maandelijks betaalt.
Om onafhankelijk van de verzekerde en het voertuig te kunnen testen, heb ik deze twee gemockt.
Ik heb getwijfeld tussen Fact en Theory, maar koos toch voor Theory, omdat ik deze eigenlijk wel handiger vind voor meerdere testen tegelijk. Zo kan ik zien bij Year en bij Month of er daadwerkelijk een verschil bestaat in expected premie en actual premie.

**WA_PLUSInsuranceCoverageCanBeCalculatedCorrectly()**

Hier heb ik getest of de WA Plus-premie inderdaad 20% duurder uitvalt dan de basispremie.
Hier heb ik ook het voertuig en de verzekerde gemockt om alleen de ‘kale’ WA-Pluspremie te kunnen testen. Uiteraard verandert de premiehoogte met het aanpassen van de voertuig- en verzekerde-waaren.
Ik heb voor een Fact gekozen voor het gemak (vergelijkbaar met het WA-premie-testgeval).

**ALL_RISKInsuranceCoverageCanBeCalculatedCorrectly()**

Dit testgeval heb ik op dezelfde manier getest als het WA_Plustestgeval hierboven.

**PostalCodeHasEffectOnPremiumAmountHight(int postalCode, double expectedPremiumAmountPerYear)**

Met dit testgeval wilde ik nagaan of alle grenswaardeanalyses van de postcode gedekt zijn. 
Ik heb Mock gebruikt voor Vehicle en Policyholder (behalve de postcode, want die onderzocht ik) omdat de rest van de variables niet direct invloed hebben op het postcodegebied.
Hier heb ik Theory gebruikt om een grenswaarde-analyse te kunnen doen met veel verschillende postcodes. Zo wil ik de ongeldige postcodes uitsluiten. 


**NoClaimYearsHaveEffectOnPremumHight(int claimYear, double expectedNoClaim)**

Met deze test wilde ik nagaan of de premie verhoogd wordt op basis van het aantal schadevrije jaren. 
Ook hier heb ik zowel het voertuig als de verzekerde gemockt, omdat ik puur de schadevrije jaren wil testen.
Hier heb ik een Theory voor gebruikt, om de grenswaarden-analyse te maken. 
 


**PolicyHoldertest**

**DrivingLicenseAgeCanBeCalculatedCorrectly(string licenseDate, int expectedLicenseYears)**

Ik heb met dit testgeval gecheckt of de rijbewijsleeftijd goed berekend wordt.
Hier speelde ik nog niet met de Mock en vulde ik willekeurig de gegevens van de verzekerde in. Ik had hier achteraf de verzekerde kunnen mocken en een setupSequence kunnen gebruiken voor het rijbewijs. Ook had ik achteraf Fact kunnen gebruiken, die was net zo effectief geweest, omdat slechts één situatie werd gecheckt, namelijk of de rijbewijsleeftijd goed meeliep met de datum van de laptop van het moment. 

**PolicyHolderAgeCanBeCalculatedCorrectly(int policyHolderYearOfBirth, int expectedAge)**

Hier heb ik de leeftijd van de verzekerde willen testen. 
Ik heb eerst de policyHolder gemockt, behalve de leeftijd. Toch werkte mijn test niet. Daarna heb ik de mock eruit gehaald. 
Ook heb ik eerst Fact geprobeerd, daarna Theory. Deze test is niet betrouwbaar, omdat ik niet kon nagaan op basis van bijvoorbeeld een geboortedatum of de leeftijd van de verzekerde wel klopt of niet. Deze test is uiteindelijk niet gelukt.


**VehicleTest**

**VehicleAgeIsCalculatedCorrectly(int constructionYear, int expectedAge)**

Deze test controleert of het autobouwjaar correct wordt berekend. 
Hier koos ik voor Theory, al kon Fact misschien hetzelfde bereiken. Ik heb eerst vehicle gedeeltelijk gemockt, maar daar ging mijn test van kapot, dus voor het gemak maar geen mock gebruikt. Hier had ik liever voor een datum in de toekomst een exception gemaakt, zodat de gebruiker een correct bouwjaar invoert, maar toch maar zo gelaten. 


**DEEL 2**

**Gevonden fouten:**

- Wat heb ik getest?
- Wat had ik verwacht?
- Waar komt deze verwachting vandaan?


**PremiumCalculationTest**

**///////////////////////////////Fout///////////////////////////////////**

  if (policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
Hier testte ik de leeftijd van de verzekerde en van het rijbewijs. Gevoelsmatig passte ik de relational operantions aan naar de volgende regel, omdat ik het 23ste jaar en het 5de rijbewijsjaar wilde dekken.

  if (policyHolder.Age <= 22 || policyHolder.LicenseAge <= 4)


**///////////////////////////////Fout///////////////////////////////////**

private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage >= 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((100d - NoClaimPercentage) / 100);
        }

Bij return heb ik bij de eerste 100 een d toegevoegd, omdat de waarde als int werd herkend en niet als double. Ook verwachtte ik bij de NoClaim van een percentage ‘gelijk’ aan 65.. Hier stond alleen een ‘> 65’. Ik heb dit aangepast naar ‘>= 65’. Toch kan ik de twee mutanten niet killen. Bij een poging de code aan te passen, verzandde ik in allerlei errors, dus heb ik de code maar weer hersteld in oude staat.

**///////////////////////////////Fout///////////////////////////////////**

private static double UpdatePremiumForPostalCode(double premium, int postalCode) => postalCode switch
        {
            >= 1000 and < 3600 => premium * 1.05,
            < 4500 => premium * 1.02,
            _ => premium,
        };

Ik paste in eerste instantie de code aan en voegde een exception toe, maar kon dit bericht niet verwerken in het testgeval “PostalCodeHasEffectOnPremiumAmountHight(int postalCode, double expectedPremiumAmountPerYear)”, naast de rest van de postcodes in de inlinedata. Ik heb dus afgezien van de errormelding en de aanpassing teruggedraaid. Ik had graag de ongeldige postcodes van een foutmelding willen voorzien.

**///////////////////////////////Fout///////////////////////////////////**

internal static double CalculateBasePremium(IVehicle vehicle)
        {
return (vehicle.ValueInEuros / 100d - vehicle.Age + vehicle.PowerInKw / 5d) / 3d;
        }

Hier ontbrak een haakje achter ‘5d’, waardoor de berekening niet meer klopte volgens het Meneer Van Dalen Wacht Op Antwoord. 
Een  ander haakje (na het woord ‘waarde’) dat in de officiële requierementsdocument (pdf) stond, had Siwa rechtgezet via een mededeling op Teams.

**PolicyHoldertest**

**///////////////////////////////Fout///////////////////////////////////**

  internal PolicyHolder(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears )
        {
            this.Age = Age;
            this.PostalCode = PostalCode;
            this.NoClaimYears = NoClaimYears;
            LicenseAge = AgeByDate(ParseDate(DriverlicenseStartDate));
        }

Als de ‘Age’ in the method een string was, dan was het makkelijk een test te houden, waarbij ik een datum invoer en een expectedleeftijd in int kan gebruiken. Ik heb de code maar zo gehouden, om die niet kapot te maken, zo vlak voor de deadline.

************************
