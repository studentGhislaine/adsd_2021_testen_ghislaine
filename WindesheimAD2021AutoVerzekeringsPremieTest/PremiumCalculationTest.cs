﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;


namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {

        [Fact]
        public void WAInsuranceCoverageCanBeCalculatedCorrectly()
        {
            //Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            double expectedBasePremium = 8.9333333333333333; // calculated with an actual calculator

            //Act
            double actualBasePremium = PremiumCalculation.CalculateBasePremium(fakeVehicle.Object);

            //Assert
            Assert.Equal(expectedBasePremium, actualBasePremium);
        }

        [Fact]
        public void PolicyHolderUnder23YearsOldGetsHigherBasePremium()
        {
            //Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(1);
            fakePolicyHolder.SetupSequence(policyHolder => policyHolder.Age)
                .Returns(22)
                .Returns(23);

            //Act
            PremiumCalculation actualBasePremiumUnder23Years = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);
            PremiumCalculation actualBasePremiumAbove23Years = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(actualBasePremiumUnder23Years.PremiumAmountPerYear, actualBasePremiumAbove23Years.PremiumAmountPerYear * 1.15);

        }

        [Fact]
        public void LicenseAgeUnder5YearsLeadsToHigherPremium()
        {
            // Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(1);
            fakePolicyHolder.SetupSequence(policyHolder => policyHolder.LicenseAge)
                .Returns(4)
                .Returns(5);

            //Act
            PremiumCalculation actualLicenseUnder5Years = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);
            PremiumCalculation actualLicenseAbove5Years = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(actualLicenseUnder5Years.PremiumAmountPerYear, actualLicenseAbove5Years.PremiumAmountPerYear * 1.15);
        }

        [Theory]
        [InlineData(PremiumCalculation.PaymentPeriod.YEAR, 8.72)] 
        [InlineData(PremiumCalculation.PaymentPeriod.MONTH, 0.74)]
        void PremiumPaymentPerYearGivesTwoPointFivePercentDiscount(PremiumCalculation.PaymentPeriod paymentPeriod, double expectedPremiumAmountPerYear)
        {
            // Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);
                                                                               
            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(1);
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);

            //Act
            PremiumCalculation premAmount = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);
            double actualpremAmountPerYear = premAmount.PremiumAmountPerYear;

            //Assert
            if (paymentPeriod == PremiumCalculation.PaymentPeriod.YEAR)
            {
                Assert.Equal(expectedPremiumAmountPerYear, premAmount.PremiumPaymentAmount(paymentPeriod));
            }
            else
            {
                Assert.Equal(expectedPremiumAmountPerYear, premAmount.PremiumPaymentAmount(paymentPeriod));
            }
        }

        [Fact]
        public void WA_PLUSInsuranceCoverageCanBeCalculatedCorrectly()  
        {
            //Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(1);
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);

            double expectedWA_PLUSPremium = 10.72;

            //Act
            PremiumCalculation actualWA_PLUSPremium = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA_PLUS);

            //Assert
            Assert.Equal(expectedWA_PLUSPremium, actualWA_PLUSPremium.PremiumAmountPerYear);
        }

        [Fact]
        public void ALL_RISKInsuranceCoverageCanBeCalculatedCorrectly() 
        {
            //Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(1);
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);

            double expectedALL_RISKPremium = 17.866666666666667; 

            //Act
            PremiumCalculation actualALL_RISKPremium = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.ALL_RISK);

            //Assert
            Assert.Equal(expectedALL_RISKPremium, actualALL_RISKPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(1000, 9.38)]
        [InlineData(3599, 9.38)]
        [InlineData(3600, 9.112)]
        [InlineData(4499, 9.112)]
        [InlineData(4500, 8.933333333333333)]
        public void PostalCodeHasEffectOnPremiumAmountHight(int postalCode, double expectedPremiumAmountPerYear)
        {
            // Arrange          
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(postalCode);
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);

            //Act
            PremiumCalculation premAmount = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);
            double actualpremAmountPerYear = premAmount.PremiumAmountPerYear;

            //Assert
            Assert.Equal(expectedPremiumAmountPerYear, actualpremAmountPerYear);
        }

        [Theory]
        [InlineData(5, 1.00)]
        [InlineData(6, 0.95)]
        [InlineData(7, 0.90)]
        [InlineData(17, 0.40)]
        [InlineData(18, 0.35)]
        [InlineData(19, 0.35)]
        public void NoClaimYearsHaveEffectOnPremumHight(int claimYear, double expectedNoClaim)
        {
            //Arrange
            var fakeVehicle = new Mock<IVehicle>();
            fakeVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(100);
            fakeVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(2680);
            fakeVehicle.Setup(vehicle => vehicle.Age).Returns(20);

            var fakePolicyHolder = new Mock<IPolicyHolder>();
            fakePolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(10);
            fakePolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(4700);
            fakePolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(30);
            fakePolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(claimYear);

            double expectedBasePremium = 8.9333333333333333;

            // Act
            PremiumCalculation premNoClaimCalc = new PremiumCalculation(fakeVehicle.Object, fakePolicyHolder.Object, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(expectedBasePremium * expectedNoClaim, premNoClaimCalc.PremiumAmountPerYear);
        }



    }
}
