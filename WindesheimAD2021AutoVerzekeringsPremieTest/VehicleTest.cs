﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;


namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(2001, 20)]
        [InlineData(2020, 1)]
        [InlineData(2025, 0)]
        public void VehicleAgeIsCalculatedCorrectly(int constructionYear, int expectedAge)
        {
           //Arrange + Act
           IVehicle actualVehicleAge = new Vehicle(100, 2680, constructionYear);           

            //Assert
            Assert.Equal(expectedAge, actualVehicleAge.Age);
        }
    }
}
