﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData("01-01-2020", 1)]
        [InlineData("01-01-2010", 11)]
        public void DrivingLicenseAgeCanBeCalculatedCorrectly(string licenseDate, int expectedLicenseYears)
        {
            //Arrange
            IPolicyHolder policyHolder = new PolicyHolder(23, licenseDate, 4700, 1);

            //Act
            int actualLicenseYears = policyHolder.LicenseAge;

            //Assert
            Assert.Equal(expectedLicenseYears, actualLicenseYears);
        }

        //[Theory]
        //[InlineData(25, 41)]
        //public void PolicyHolderAgeCanBeCalculatedCorrectly(int policyHolderAge, int expectedAge)
        //{
        //    //Arrange
        //    IPolicyHolder policyHolder = new PolicyHolder(policyHolderAge, "05-01-2015", 4700, 4);

        //    //Act
        //    int actualAge = policyHolder.Age;

        //    //Assert
        //    Assert.Equal(expectedAge, actualAge);
        //}
    }
}
